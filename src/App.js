import React from 'react';
import logo from './logo.svg';
import './App.css';
import ServerSideDatatable from "./components/ServerSideDatatable";

function App() {
    return (
        <div className="container py-4">
            <div className="text-center">
                <h4>React JS Datatable Server Side</h4>
            </div>
            <div className="mt-4">
                <ServerSideDatatable />
            </div>
        </div>
    );
}

export default App;
