import React, {Component} from 'react';
import axios from 'axios';
import SolanumPagination from "../components/SolanumPagination";

class Home extends Component {
    constructor(props) {
        super(props);
        /* An example array of 150 items to be paged */
        let exampleItems = [...Array(150).keys()].map(i => ({id: (i + 1), name: 'Item ' + (i + 1)}));
        console.log('exampleItems', exampleItems);
        this.state = {
            exampleItems: exampleItems,
            pageOfItems: [],
            data: [],
            current_page: 0,
            last_page: 0,
        };
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_BASE_API_URL}test`).then(
            response => {
                console.log(response.data.data);
                this.setState({
                    data: response.data.data.data,
                    current_page: response.data.data.current_page,
                    last_page: response.data.data.last_page
                });
            }
        ).catch(error => {
            console.error(error);
        });
    }

    onChangePage(pageOfItems, page) {
        axios.get(`${process.env.REACT_APP_BASE_API_URL}test?page=` + page).then(
            response => {
                console.log(response.data.data);
                // this.setState({
                //     data: response.data.data.data
                // });
            }
        ).catch(error => {
            console.error(error);
        });
        /* Update state with new page of items */
        this.setState({pageOfItems: pageOfItems});
    }

    render() {
        return (
            <div>
                {this.state.pageOfItems.map(item =>
                    <div key={item.id}>{item.name}</div>
                )}
                <div className="d-flex justify-content-center mt-5">
                    <SolanumPagination
                        items={this.state.data}
                        onChangePage={this.onChangePage}
                        currentPage={this.state.current_page}
                        lastPage={this.state.last_page}
                    />
                </div>
            </div>
        );
    }
}

export default Home;