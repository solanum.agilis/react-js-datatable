import React, { Component } from 'react';
import ReactDatatable from '@ashvin27/react-datatable';
import axios from 'axios';
import Config from "./axios/Config";

class ServerSideDatatable extends Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "id",
                text: "Id",
                className: "name",
                sortable: true
            },
            {
                key: "name",
                text: "Name",
                sortable: true
            },
            {
                key: "image",
                text: "Image",
                cell: (record, index) => (
                    <div className="text-center">
                        <img src={record.image} alt="" width={25} height={25} />
                    </div>
                )
            },
            {
                key: "created_at",
                text: "Created At",
                sortable: true
            },
            {
                key: "updated_at",
                text: "Updated At",
                sortable: true
            }
        ];
        this.config = {
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            pagination: 'advance',
            button: {
                excel: false,
                print: false
            }
        }
        this.state = {
            total: 0,
            records: []
        }
        this.getData = this.getData.bind(this);
    }

    componentDidMount(){
        this.getData();
    }

    getData(data = null) {
        console.log(data);
        let url;
        if (data === null) url = `${process.env.REACT_APP_BASE_API_URL}test?length=10`;
        else url = `${process.env.REACT_APP_BASE_API_URL}test?search=${data.filter_value}&column=${data.sort_order ? data.sort_order.column : ''}&dir=${data.sort_order ? data.sort_order.order : ''}&length=${data.page_size}&page=${data.page_number}`;
        axios.get(url, Config()).then(
            response => {
                console.log(response.data.data);
                this.setState({
                    total: response.data.data.total,
                    records: response.data.data.data
                });
            }
        ).catch(error => {
            console.error(error);
        });
    }

    render() {
        return (
            <ReactDatatable
                config={this.config}
                records={this.state.records}
                columns={this.columns}
                dynamic={true}
                total_record={this.state.total}
                onChange={this.getData} />
        );
    }
}

export default ServerSideDatatable;