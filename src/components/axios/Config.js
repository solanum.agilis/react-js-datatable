import React, {Component} from 'react';
import {loadProgressBar} from "axios-progress-bar";
// import 'axios-progress-bar/dist/nprogress.css';
import './ProgressBar.css';

const Config = (headers) => {
    return {
        onUploadProgress: progressEvent => {
            loadProgressBar();
        },
        onDownloadProgress: progressEvent => {
            loadProgressBar();
        },
        headers: headers
    }
};

export default Config;